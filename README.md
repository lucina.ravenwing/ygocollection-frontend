# YuGiOh Collection Frontend (WIP)

[![Build Status](https://jenkins.lucinaravenwing.net/buildStatus/icon?job=ygo-coll-frontend&style=plastic)](https://jenkins.lucinaravenwing.net/job/ygo-coll-frontend/)

## Repository link
https://gitlab.com/lucina.ravenwing/ygocollection-frontend

## URL
https://www.ygo.lucinaravenwing.net

## Description
The frontend for a Yu-Gi-Oh collection app. The application allows users to compile a list of cards that they own from the Yu-Gi-Oh trading card game and view the approximate worth of their collection. They are also able to build decks that they would like to put together and pull from their collection to approximate how much the deck will cost to build or complete. If the user is logged in, they can save the collection and decks, but are also able to do both activities as a guest, but are unable to save their work.</br>
The front end calls into the back end API to retrieve card information and relay the information to the users. It uses ReactJS to serve the information as a single page application.

## Languages
Javascript

## Resources Used
ReactJS\
CI/CD pipeline\
Docker\
nginx

==================