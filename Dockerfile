#FROM node:lts-bullseye-slim AS build
#
#WORKDIR /app
#
#ADD ./build/. .
#
#RUN npm install -g serve
#CMD serve /app
#
#FROM nginx:stable-alpine
#
#WORKDIR /app
#
#COPY --from=build /app .
#
#COPY /nginx/default /etc/nginx/sites-available/
#
#EXPOSE 3000

FROM node:lts-bullseye-slim AS build

WORKDIR /app

ADD ./build/. .

FROM nginx:stable-alpine

COPY --from=build /app /usr/share/nginx/html

COPY /nginx/default.conf /etc/nginx/conf.d/

CMD ["nginx", "-g", "daemon off;"]