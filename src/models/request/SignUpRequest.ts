export interface SignUpRequest {
    Username: string,
    EmailAddress: string,
    Password: string,
    ConfirmPassword: String
}