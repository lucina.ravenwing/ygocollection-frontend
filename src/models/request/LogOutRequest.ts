export interface LogOutRequest {
    RefreshToken: string | null
}