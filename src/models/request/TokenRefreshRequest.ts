export interface TokenRefreshRequest {
    Token: string,
    RefreshToken: string
}