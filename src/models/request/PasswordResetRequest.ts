export interface PasswordResetRequest {
    Email: string,
    NewPassword: string,
    Origin: string,
    Token: string
}