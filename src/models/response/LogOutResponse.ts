export interface LogOutResponse {
    message: string,
    token: string,
    refreshToken: string,
    expiresAt: Date
}