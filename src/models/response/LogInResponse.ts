export interface LogInResponse {
    message: string,
    token: string,
    refreshToken: string,
    expiresAt: Date
}