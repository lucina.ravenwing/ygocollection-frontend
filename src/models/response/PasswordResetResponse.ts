export interface PasswordResetResponse {
    message: string,
    token: string,
    refreshToken: string,
    expiresAt: string
}