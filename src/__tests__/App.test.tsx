import React from 'react';
import {render, screen} from '@testing-library/react';
import App from '../components/App';
import {BrowserRouter} from "react-router-dom";

test('renders learn react link', () => {
    render(<App/>, {wrapper: BrowserRouter});
    const linkElement = screen.getByText(/YGO Collection/i);
    expect(linkElement).toBeInTheDocument();
});