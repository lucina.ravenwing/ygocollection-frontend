pipeline {
    agent { label 'ygo-coll-frontend' }
    stages {
        stage('build') {
            steps{
                updateGitlabCommitStatus name: 'build', state: 'running'
                echo 'Starting build stage.'

                // Check if anything needs to be copied from server here for security purposes.
                sh 'npm ci'

                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }
        stage('test') {
            steps {
                updateGitlabCommitStatus name: 'test', state: 'running'
                echo 'Starting test stage.'

                sh 'npm run test'

                updateGitlabCommitStatus name: 'test', state: 'success'
            }
        }
        stage('release') {
            steps {
                updateGitlabCommitStatus name: 'release', state: 'running'
                echo 'Starting release stage. Includes:\n\t- Archive the old release folder.\n\t- Publish the new release folder. \n\t- Build the docker image locally.\n\t- Push the image to Docker Hub.'

                sh 'npm run build'

                // Archive the old release folder and move new folder to the correct location. mv command ends with or true in case the original folder doesn't exist.
                sh 'mv /home/jenkins/deploy-react/* /home/jenkins/archived-react || true'
                sh 'cp -r ./build /home/jenkins/deploy-react || true'

                // Docker deploy steps. Both have an or true in case the docker containers on the server get cleaned out.
                sh 'docker stop ygo-collection-frontend || true'
                sh 'docker rm ygo-collection-frontend || true'

                // Push image to DockerHub
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'jenkins-dockerhub') {
                        app = docker.build("lucinaravenwing/ygo-collection-frontend")
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }
                }

                updateGitlabCommitStatus name: 'release', state: 'success'
            }
        }
        stage('deploy') {
            steps {
                updateGitlabCommitStatus name: 'deploy', state: 'success'
                echo 'Starting deploy stage. Includes:\n\t- Running the docker container.'

//                 sh 'docker run -d -it --network=host --name=ygo-collection-frontend -v logs:/logs lucinaravenwing/ygo-collection-frontend'
                sh 'docker run -d -it -p 127.0.0.1:3001:3000 --name=ygo-collection-frontend -v logs:/logs lucinaravenwing/ygo-collection-frontend'

                sh 'pwd'
                updateGitlabCommitStatus name: 'deploy', state: 'success'
            }
        }
    }
}